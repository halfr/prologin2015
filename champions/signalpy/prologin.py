# -*- coding: utf-8 -*-

import api
from api import *

import signal

done = False

def handler(signum, frame):
    print('Signal handler called with signal', signum)
    global done
    done = True

# Set the signal handler and a 5-second alarm
signal.signal(signal.SIGTERM, handler)

# Fonction appelée au début de la partie
def partie_init():
    pass

# Fonction appelée à chaque tour
def jouer_tour():
    global done
    done = False
    while not done:
        pass

# Fonction appelée à la fin de la partie
def partie_fin():
    pass # Place ton code ici


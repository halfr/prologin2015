///
// This file has been generated, if you wish to
// modify it in a permanent way, please refer
// to the script file : gen/generator_cxx.rb
//

#include "prologin.hh"

#include <iostream>
#include <signal.h>
#include <unistd.h>

///
// Fonction appelée au début de la partie
//
void partie_init()
{
  // fonction a completer
}

bool signaled;

void sigaction_handler(int sig)
{
    std::cerr << "signal caught" << std::endl;
    signaled = true;
}

///
// Fonction appelée à chaque tour
//
void jouer_tour()
{
    struct sigaction sa;

    signaled = false;

    sa.sa_handler = sigaction_handler;
    sigemptyset(&sa.sa_mask);

    sigaction(SIGTERM, &sa, NULL);

    while (!signaled)
        usleep(50000u);

    std::cout << "return" << std::endl;
}

///
// Fonction appelée à la fin de la partie
//
void partie_fin()
{
  // fonction a completer
}

